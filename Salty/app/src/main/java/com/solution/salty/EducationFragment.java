package com.solution.salty;
import android.os.Bundle;
    
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class EducationFragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager2 myViewPager2;
    private ViewPagerFragmentAdapter myAdapter;
    private ArrayList<Fragment> arrayList;

    private static final String[] barName = new String[]{"Society","Biology","Geography"};

    public EducationFragment() {
        arrayList = new ArrayList<>();
    }

    public static EducationFragment newInstance() {
        EducationFragment fragment = new EducationFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_education, container, false);
        initTabLayout(view);

        ViewPager2.OnPageChangeCallback pageChangeCallback = new MyOnPageChangeCallback(tabLayout);

        setupViewPager2AndTabLayout(pageChangeCallback);

        return view;
    }

    /**
     * configuration of the OnPage of the view with respective settings of the navigation bar
     * @param pageChangeCallback reference to the navigation bar*/
    private void setupViewPager2AndTabLayout(ViewPager2.OnPageChangeCallback pageChangeCallback) {
        myAdapter = new ViewPagerFragmentAdapter(getActivity().getSupportFragmentManager(), getLifecycle());

        myViewPager2.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);

        myViewPager2.setAdapter(myAdapter);
        myViewPager2.registerOnPageChangeCallback(pageChangeCallback);
        tabLayout.addOnTabSelectedListener(new MyOnTabSelectedListener(myViewPager2));
    }

    /**
     * Initialization of the TabLayout for view education
     * @param view a view of fragment education
     * */
    private void initTabLayout(View view) {
        myViewPager2 = view.findViewById(R.id.view_pager);
        tabLayout = view.findViewById(R.id.tabs);
        for(int i=0;i<barName.length;++i)
            tabLayout.addTab(tabLayout.newTab().setText(barName[i]));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        arrayList.add(new SocietyFragment());
        arrayList.add(new BiologyFragment());
        arrayList.add(new GeographyFragment());
    }
}