package com.solution.salty;

import android.view.View;


class MyGPSOnClickListener implements View.OnClickListener {

    private MyOnMapReadyCallback myOnMapReadyCallback;

    public MyGPSOnClickListener(MyOnMapReadyCallback myOnMapReadyCallback){
        this.myOnMapReadyCallback = myOnMapReadyCallback;
    }

    /**
     * GPS status check with relative activation/deactivation
     * @param v iteration with the view, inherited from the parent class method*/
    @Override
    public void onClick (View v){
        if (myOnMapReadyCallback.stateGPS()){
            myOnMapReadyCallback.setGPSOff();
        } else {
            myOnMapReadyCallback.setGPSOn();
        }
    }

}
