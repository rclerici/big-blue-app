package com.solution.salty;

import android.app.Activity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.common.hash.Hashing;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

import info.guardianproject.netcipher.proxy.OrbotHelper;

public class TorReport {
    private String[] server_response;
    private String prefix;
    private String message;
    private boolean tor;
    private Activity activity;
    private View view;
    private String[] fieldName;

    public TorReport(boolean tor,Activity activity,View v){
        server_response = new String[3];
        this.tor=tor;
        this.activity=activity;
        view=v;
        fieldName=new String[3];
        if(tor){
            prefix="http://wrwdnzywapscf24cwamspi7c7wuprab2ndbunxklbxdcp3gevua4jkqd.onion/api";
            this.checkTor();
        }else {
            prefix = "http://odroid/api";
        }
    }
    public void sendMessage(String message){
        this.message=message;
        ProgressBar circle= view.findViewById(R.id.progress_bar);
        circle.setVisibility(View.VISIBLE);
        getPublic();
    }
    private void getPublic() {
        final RequestQueue queue;
        if (tor) {
            queue = Volley.newRequestQueue(activity.getApplicationContext(), new ProxiedHurlStack());
        } else {
            queue = Volley.newRequestQueue(activity.getApplicationContext());
        }
        String url = prefix + "/public";
        JsonObjectRequest jsonRequest = new JsonObjectRequest(
                Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray questionnaire=response.getJSONArray("questionnaires");
                            JSONObject defaultQ=questionnaire.getJSONObject(0);
                            JSONArray steps=defaultQ.getJSONArray("steps");
                            JSONArray children=steps.getJSONObject(0).getJSONArray("children");
                            for(int i=0;i<children.length();i++){
                                fieldName[i]=children.getJSONObject(i).getString("id");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        getToken(queue);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        ProgressBar circle= view.findViewById(R.id.progress_bar);
                        circle.setVisibility(View.GONE);
                        Toast toast = Toast.makeText(activity.getApplicationContext(),
                                "connection error",
                                Toast.LENGTH_SHORT);
                        toast.show();

                    }
                }

        );
        queue.add(jsonRequest);
    }
    private void getToken(RequestQueue queue) {
            JSONObject jsonBody = null;
            try {
                jsonBody = new JSONObject("{}");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            String url = prefix + "/token";
            JsonObjectRequest jsonRequest = new JsonObjectRequest(
                    Request.Method.POST, url, jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                server_response[0] = response.getString("id");
                                server_response[1] = response.getString("creation_date");
                                server_response[2] = response.getString("ttl");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            okToken(queue);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            ProgressBar circle= view.findViewById(R.id.progress_bar);
                            circle.setVisibility(View.GONE);
                            Toast toast = Toast.makeText(activity.getApplicationContext(),
                                    "errore metodo getToken",
                                    Toast.LENGTH_SHORT);
                            toast.show();

                        }
                    }

            );
            queue.add(jsonRequest);
        }
    private void okToken(final RequestQueue queue){
        JSONObject jsonBody2 = new JSONObject();
        String url=prefix+"/token/";
        try {
            jsonBody2.put("id", server_response[0]);
            jsonBody2.put("creation_date", server_response[1]);
            jsonBody2.put("ttl", Integer.parseInt(server_response[2]));
            jsonBody2.put("answer",answer(server_response[0]));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println(jsonBody2.toString());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(
                Request.Method.PUT, url+ server_response[0], jsonBody2,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do something with the response
                        System.out.println(response.toString());
                        submission(queue);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // do something with the error
                        error.printStackTrace();
                        ProgressBar circle= view.findViewById(R.id.progress_bar);
                        circle.setVisibility(View.GONE);
                        Toast toast = Toast.makeText(activity.getApplicationContext(),
                                "errore conferma token",
                                Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }

        );
        queue.add(jsonRequest);
    }
    private void submission(final RequestQueue queue){
        String url=prefix+"/submission/";
        JSONObject jsonBody3 = new JSONObject();
        JSONArray receivers= new JSONArray();
        JSONObject answers=new JSONObject();
        JSONObject oggetto=new JSONObject();

        JSONObject oggetto1=new JSONObject();

        JSONObject oggetto2=new JSONObject();
        JSONArray array1= new JSONArray();
        JSONArray array2= new JSONArray();
        JSONArray array3= new JSONArray();

        try {
            receivers.put("c2dd4345-ddda-4426-8f11-5e29083ee189");

            oggetto.put("required_status",false);
            oggetto.put("value","InSabbia");
            oggetto1.put("required_status",false);
            oggetto1.put("value",message);
            oggetto2.put("required_status",false);

            array1.put(oggetto);
            array2.put(oggetto1);
            array3.put(oggetto2);


            answers.put(fieldName[0],array1);
            answers.put(fieldName[1],array2);
            answers.put(fieldName[2],array3);

            jsonBody3.put("context_id","143d10f6-b862-4a18-83ed-8456a1fe472a");
            jsonBody3.put("receivers",receivers);
            jsonBody3.put("identity_provided",false);
            jsonBody3.put("answers", answers);
            jsonBody3.put("answer", 0);
            jsonBody3.put("total_score", 0);
            jsonBody3.put("removed_files", new JSONArray());
            jsonBody3.put("token_id", server_response[0]);
            /*jsonBody3=new JSONObject("{\"context_id\":\"24c7ddc2-91df-4b8d-8f2f-53d37312b6dc\"," +
                    "\"receivers\":[\"72ef23eb-f945-4f5a-9e3a-763b1539b812\"]," +
                    "\"identity_provided\":false," +
                    "\"answers\":{\"557d51ef-7d02-46bb-9cac-f356feae40f6\":[{\"required_status\":false," +
                    "\"value\":\"telefono\"}]," +
                    "\"d75f0607-f1a0-446e-9e92-8c1dea870457\":[{\"required_status\":false," +
                    "\"value\":\"tizio&caio\"}]," +
                    "\"86694f23-bdd8-4408-8912-48917cea4e11\":[{\"required_status\":false}]}," +
                    "\"answer\":0," +
                    "\"total_score\":0," +
                    "\"removed_files\":[]," +
                    "\"token_id\":\""+server_response[0]+"\"}");*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println(jsonBody3.toString());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(
                Request.Method.PUT, url+ server_response[0], jsonBody3,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do something with the response
                        try {
                            ProgressBar circle= view.findViewById(R.id.progress_bar);
                            circle.setVisibility(View.GONE);
                            System.out.println(response.toString());
                            Toast toast = Toast.makeText(activity.getApplicationContext(),
                                    "ricevuta:" + response.getString("receipt"),
                                    Toast.LENGTH_LONG);
                            toast.show();
                            receiptDialog(response.getString("receipt"));

                        }catch(JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // do something with the error
                        ProgressBar circle= view.findViewById(R.id.progress_bar);
                        circle.setVisibility(View.GONE);
                        error.printStackTrace();
                        Toast toast = Toast.makeText(activity.getApplicationContext(),
                                "errore invio submission",
                                Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }

        );
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(240000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsonRequest);
    }
    private int answer(String token){
        int i=0;
        String sha256hex;
        do {
            String temp = token + i ;
            byte[] bytes = temp.getBytes(StandardCharsets.UTF_8);
            String utf8EncodedString = new String(bytes, StandardCharsets.UTF_8);
            sha256hex = Hashing.sha256()
                    .hashString(utf8EncodedString, StandardCharsets.UTF_8)
                    .toString();
            i++;
        }while(!sha256hex.endsWith("00"));
        i--;
        return i;
    }
    private void checkTor(){
        if (!OrbotHelper.get(activity.getApplicationContext()).init()) {
            OrbotHelper.get(activity.getApplicationContext()).installOrbot(activity);
        }

    }
    private void receiptDialog(String message){
        new AlertDialog.Builder(activity)
                .setTitle("Ricevuta")
                .setMessage(message)
                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }


}
