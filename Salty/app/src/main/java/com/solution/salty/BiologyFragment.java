package com.solution.salty;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class BiologyFragment extends Fragment {


    public BiologyFragment() {
    }
    public static BiologyFragment newInstance() {
        return (new BiologyFragment());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_biology, container, false);
        initLinkPhoto(v);
        return v;
    }

    /**
     * Configuration of links for reference of articles or photos in the education section, in particular Biology
     * @param v a reference view for the links present*/
    private void initLinkPhoto(View v) {
        TextView linkPhoto4 = (TextView) v.findViewById(R.id.linkPhoto4);
        linkPhoto4.setMovementMethod(LinkMovementMethod.getInstance());

        TextView linkPhoto5 = (TextView) v.findViewById(R.id.linkPhoto5);
        linkPhoto5.setMovementMethod(LinkMovementMethod.getInstance());

        TextView linkPhoto6 = (TextView) v.findViewById(R.id.linkPhoto6);
        linkPhoto6.setMovementMethod(LinkMovementMethod.getInstance());
    }

}