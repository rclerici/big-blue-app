package com.solution.salty;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class ViewPagerFragmentAdapter extends FragmentStateAdapter {

    private static final int ITEM_COUNT = 3;

    public ViewPagerFragmentAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    /**
     * Creation and selection based on the user selection of the interested view in the education section
     * @param position an integer value to specify which section you want to show
     * @return the object of the selected section*/
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new SocietyFragment();
            case 1:
                return new BiologyFragment();
            case 2:
                return new GeographyFragment();

        }
        return null;
    }

    @Override
    public int getItemCount() {
        return ITEM_COUNT;
    }
}