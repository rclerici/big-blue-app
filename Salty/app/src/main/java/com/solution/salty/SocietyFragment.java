package com.solution.salty;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
public class SocietyFragment extends Fragment {

    public SocietyFragment() { }
    public static SocietyFragment newInstance() {
        return (new SocietyFragment());
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_society, container, false);
        initLinks(v);
        return v;
    }

    /**
     * Configuration of links for reference of articles or photos in the education section, in particular Society
     * @param v a reference view for the links present*/
    private void initLinks(View v) {
        TextView linkPhoto2 = (TextView) v.findViewById(R.id.linkPhoto2);
        linkPhoto2.setMovementMethod(LinkMovementMethod.getInstance());

        TextView linkPhoto3 = (TextView) v.findViewById(R.id.linkPhoto3);
        linkPhoto3.setMovementMethod(LinkMovementMethod.getInstance());

        TextView sourceLink = (TextView) v.findViewById(R.id.sourceLink);
        sourceLink.setMovementMethod(LinkMovementMethod.getInstance());
    }

}