package com.solution.salty;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;

public class ReportFragment extends Fragment {

    private static MapView mapView;
    private static Activity activity = null;

    public ReportFragment() {}

    public static ReportFragment newInstance() {
        ReportFragment fragment = new ReportFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        Mapbox.getInstance(activity.getApplicationContext(), getString(R.string.token_key));
        View v = inflater.inflate(R.layout.fragment_report,container,false);
        initReport(savedInstanceState, v);
        return v;
    }

    /**
     *Map configuration with the respective Mapbox key and GPS signaling and activation buttons
     *@param savedInstanceState current state
     *@param v current view state*/
    private void initReport(Bundle savedInstanceState, View v) {
        mapView = (MapView) v.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        MyOnMapReadyCallback myOnMapReadyCallback = new MyOnMapReadyCallback(mapView,activity);
        mapView.getMapAsync(myOnMapReadyCallback);
        initButton(v,myOnMapReadyCallback);
    }

    /**
     *Button initialisation: reporting and GPS
     *@param v currect view state
     *@param myOnMapReadyCallback currect map state*/
    private static void initButton(View v, MyOnMapReadyCallback myOnMapReadyCallback){
        ImageButton gpsButton = (ImageButton) v.findViewById(R.id.gps);
        gpsButton.setOnClickListener(new MyGPSOnClickListener(myOnMapReadyCallback));

        ImageButton reportButton = (ImageButton) v.findViewById(R.id.reportMessage);
        reportButton.setOnClickListener(new MyReportOnClickListener(activity,myOnMapReadyCallback,v));
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }
}