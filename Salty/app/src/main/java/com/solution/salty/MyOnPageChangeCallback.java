package com.solution.salty;

import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;

public class MyOnPageChangeCallback extends ViewPager2.OnPageChangeCallback {

    private TabLayout tabLayout;

    public MyOnPageChangeCallback(TabLayout tabLayout){
        this.tabLayout = tabLayout;
    }

    @Override
    public void onPageSelected(int position) {
        super.onPageSelected(position);
        TabLayout.Tab tab=tabLayout.getTabAt(position);
        tab.select();
    }
}
