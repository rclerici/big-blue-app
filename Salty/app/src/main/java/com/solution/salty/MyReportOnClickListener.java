package com.solution.salty;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.mapbox.mapboxsdk.geometry.LatLng;

import org.jetbrains.annotations.NotNull;


import java.util.Arrays;
import java.util.Date;
import java.util.List;


class MyReportOnClickListener extends AppCompatDialogFragment implements View.OnClickListener{

    private MyOnMapReadyCallback myOnMapReadyCallback;
    private Activity activity;
    private static Spinner typeSand,sizeSand,colorSand;
    private TorReport report;

    public MyReportOnClickListener(Activity activity, MyOnMapReadyCallback myOnMapReadyCallback,View v){
        this.activity = activity;
        this.myOnMapReadyCallback = myOnMapReadyCallback;
        report=new TorReport(false,activity,v);
    }

    /**
     *creation of an alert message in the event of an alert or not
     *@param saveInstanceState current state
     *@return window with respective message*/
    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState){
        AlertDialog.Builder builder;
        if(myOnMapReadyCallback.stateGPS()||myOnMapReadyCallback.stateSymbolOptions())
            builder = initAlertDialogReportOk(activity);
        else
            builder = initAlertDialogWarning(activity);

        return builder.create();
    }
    @Override
    public void onClick(View v) {
        onCreateDialog(getArguments()).show();
    }

    /**
     *Initialization and configuration of the message to be sent with the respective point that the user is selected on the map
     *@param activity current activity state
     *@return window with respective mesvisibility gone invisible difference    sage*/
    private AlertDialog.Builder initAlertDialogReportOk(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_typesand,null);

        initSpinner(v,activity);

        builder.setView(v).setMessage("Complete the report")
                .setTitle("Ready to send")
                .setPositiveButton("Send", myDialogOnClickListenerOkPositiveButton)
                .setNegativeButton("Cancel", myDialogOnClickListenerOkNegativeButton);
        return builder;

    }

    /**
     *Initialization and configuration of the lists for signaling: type, size and color sand
     *@param v current view state
     *@param activity current activity state*/
    private static void initSpinner(View v, Activity activity) {
        List<String> arrayTypeSand = Arrays.asList(activity.getResources().getStringArray(R.array.type_sand));
        List<String> arraySizeSand = Arrays.asList(activity.getResources().getStringArray(R.array.size_sand));
        List<String> arrayColorSand = Arrays.asList(activity.getResources().getStringArray(R.array.color_sand));

        typeSand = (Spinner) v.findViewById(R.id.spinner_typesand);
        ArrayAdapter<String> adapterTypeSand = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_item,arrayTypeSand);
        adapterTypeSand.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSand.setAdapter(adapterTypeSand);

        sizeSand = (Spinner) v.findViewById(R.id.spinner_sizesand);
        ArrayAdapter<String> adapterSizeSand = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_item,arraySizeSand);
        adapterSizeSand.setDropDownViewResource(android.R.layout.simple_spinner_item);
        sizeSand.setAdapter(adapterSizeSand);

        colorSand = (Spinner) v.findViewById(R.id.spinner_colorsand);
        ArrayAdapter<String> adapterColorSand = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_item,arrayColorSand);
        adapterColorSand.setDropDownViewResource(android.R.layout.simple_spinner_item);
        colorSand.setAdapter(adapterColorSand);
    }

    /**
     *Attention message
     *@param activity current activity state
     *@return window with the relative message*/
    private AlertDialog.Builder initAlertDialogWarning(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        return builder.setMessage(R.string.dialog_message_no)
                .setTitle(R.string.dialog_title_no)
                .setCancelable(false)
                .setNegativeButton("Ok", myDialogOnClickListenerNoNegativeButton);
    }

    private DialogInterface.OnClickListener myDialogOnClickListenerOkPositiveButton = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);


            String plainMessage=createMessage(new Spinner[]{typeSand,sizeSand,colorSand});
            report.sendMessage(plainMessage);
        }
    };



    private DialogInterface.OnClickListener myDialogOnClickListenerOkNegativeButton = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
            Toast.makeText(activity, "Canceled report",
                    Toast.LENGTH_SHORT).show();
        }
    };

    private DialogInterface.OnClickListener myDialogOnClickListenerNoNegativeButton = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
        }
    };

    /**
     *Format of the message to be sent to the server
     *@param spinners optional parameters to add to the final message
     *@return message to send to the server*/
    private String createMessage(Spinner[] spinners){
        StringBuilder stringBuilder;
        double longt,lat;
        LatLng position;
        Date d = new Date(System.currentTimeMillis());
        position = myOnMapReadyCallback.stateGPS() ? myOnMapReadyCallback.getPositionGPS() : myOnMapReadyCallback.getPositionSelected();
        longt = position.getLongitude();
        lat = position.getLatitude();
        stringBuilder = new StringBuilder();
        stringBuilder.append(lat+";");
        stringBuilder.append(longt+";");
        for (Spinner s: spinners)
            stringBuilder.append((String)s.getSelectedItem()+";");
        return stringBuilder.append(d.toString()).toString();
    }
}
