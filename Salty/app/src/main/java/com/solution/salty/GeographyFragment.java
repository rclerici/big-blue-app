package com.solution.salty;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class GeographyFragment extends Fragment {


    public GeographyFragment() {
    }
    public static GeographyFragment newInstance() {
        return (new GeographyFragment());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_geography, container, false);
        initLink(v);
        return v;
    }

    /**
     * Configuration of links for reference of articles or photos in the education section, in particular Geography
     * @param v a reference view for the links present*/
    private void initLink(View v) {
        TextView linkPhoto7 = (TextView) v.findViewById(R.id.linkPhoto7);
        linkPhoto7.setMovementMethod(LinkMovementMethod.getInstance());

        TextView linkPhoto8 = (TextView) v.findViewById(R.id.linkPhoto8);
        linkPhoto8.setMovementMethod(LinkMovementMethod.getInstance());

        TextView linkArticole = (TextView) v.findViewById(R.id.linkArticole);
        linkArticole.setMovementMethod(LinkMovementMethod.getInstance());

        TextView linkPhoto9 = (TextView) v.findViewById(R.id.linkPhoto9);
        linkPhoto9.setMovementMethod(LinkMovementMethod.getInstance());
    }

}