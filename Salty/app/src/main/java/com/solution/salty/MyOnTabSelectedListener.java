package com.solution.salty;

import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;

public class MyOnTabSelectedListener implements TabLayout.OnTabSelectedListener {

    private ViewPager2 viewPager2;

    public MyOnTabSelectedListener(ViewPager2 viewPager2){
        this.viewPager2 = viewPager2;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        viewPager2.setCurrentItem(position);
    }
    @Override
    public void onTabUnselected(TabLayout.Tab tab) {}

    @Override
    public void onTabReselected(TabLayout.Tab tab) {}
}
