package com.solution.salty;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;

import java.util.List;

class MyOnMapReadyCallback implements OnMapReadyCallback, PermissionsListener {

    private MapView mapView;
    private Activity activity;
    private static LocationComponent locationComponent;
    private static MapboxMap mapboxMap;
    private static Style style;
    private static Symbol symbolOptions;
    private static SymbolManager symbolManager;
    private static boolean isActiviteGPS;

    public MyOnMapReadyCallback(MapView mapView, Activity activity) {
        this.mapView = mapView;
        this.activity = activity;
    }

    /**
     * Satellite map configuration using Mapbox tool and state GPS
     * @param mapboxMap reference the object*/
    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        isActiviteGPS = false;
        mapboxMap.setStyle(Style.SATELLITE_STREETS,new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                MyOnMapReadyCallback.style = style;
                symbolManager = new SymbolManager(mapView,mapboxMap,style);

                symbolManager.setIconAllowOverlap(true);
                symbolManager.setTextAllowOverlap(true);

                enableLocationComponent(mapboxMap,style);
            }
        });
        mapboxMap.addOnMapLongClickListener(new MapboxMap.OnMapLongClickListener() {
            /**
             * Pressure action on the map to obtain an animation of a flag for user-specified
             * @param point coordinates of the user generated point
             * @return always returns true*/
            @Override
            public boolean onMapLongClick(@NonNull LatLng point) {
                deleteSymbol();
                symbolOptions = symbolManager.create(
                        new SymbolOptions().withLatLng(point).
                                withIconSize(2.0f)
                                .withDraggable(false).withIconImage("embassy-15"));
                symbolManager.update(symbolOptions);
                CameraPosition position = new CameraPosition.Builder().target(point).build();
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
                setGPSOff();
                return true;
            }
        });

        mapboxMap.addOnMapClickListener(new MapboxMap.OnMapClickListener() {
            /**
             * Elimination of the flag on the map with a simple click
             * @param point coordinated at the point clicked on the map
             * @return always returns true*/
            @Override
            public boolean onMapClick(@NonNull LatLng point) {
                deleteSymbol();
                return true;
            }
        });

        mapboxMap.getUiSettings().setAttributionEnabled(false);
        mapboxMap.getUiSettings().setLogoEnabled(false);

    }

    @SuppressWarnings({"MissingPermission"})
    /**
     * Enable GPS service
     * @param mapboxMap reference to the map
     * @param style reference to the selected style of the map that makes Mapbox available*/
    private void enableLocationComponent(MapboxMap mapboxMap, Style style) {
        if(PermissionsManager.areLocationPermissionsGranted(activity.getApplicationContext())){
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(LocationComponentActivationOptions.builder(activity.getApplicationContext(), style).build());
            locationComponent.setLocationComponentEnabled(isActiviteGPS);
            locationComponent.setCameraMode(CameraMode.TRACKING_COMPASS);
            locationComponent.setRenderMode(RenderMode.GPS);

        } else {
            PermissionsManager permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(activity);
        }
    }
    /**
     * Effective elimination of the symbol if present on the map*/
    private static void deleteSymbol(){
        if(symbolOptions!=null){
            symbolManager.delete(symbolOptions);
            symbolOptions = null;
        }
    }

    /**
     * GPS activation after proper configuration*/
    public void setGPSOn(){
        if(locationComponent!=null){
            isActiviteGPS = true;
            deleteSymbol();
            try{
                locationComponent.setLocationComponentEnabled(true);
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition
                        (new CameraPosition.Builder().zoom(10d).target(getPositionGPS()).build())
                );
            }catch(Exception e){
                isActiviteGPS = false;
                buildAlertMessageNoGps();
            }
        }else
            this.enableLocationComponent(mapboxMap,style);
    }

    /**
     * Realization of an alert message if the GPS service is not active*/
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        activity.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean stateGPS(){
        return isActiviteGPS;
    }

    public boolean stateSymbolOptions(){
        return symbolOptions != null;
    }
    /**
     * GPS deactivation*/
    public void setGPSOff(){
        if(locationComponent!=null){
            isActiviteGPS = false;
            locationComponent.setLocationComponentEnabled(false);
        }
    }

    /**
     * I get the position that was selected by pressing on the map
     * @return coordinates of the selected point
     * @exception NullPointerException in case no flag has been placed*/
    public LatLng getPositionSelected() throws NullPointerException{
        return symbolOptions.getLatLng();
    }

    /**
     * I get the coordinates that were obtained from the GPS activation
     * @return coordinates GPS
     * @exception NullPointerException in case it is not active*/
    public LatLng getPositionGPS() throws NullPointerException{
        return new LatLng(locationComponent.getLastKnownLocation().getLatitude(),locationComponent.getLastKnownLocation().getLongitude());
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        String allPermission="";
        for (String a: permissionsToExplain)
            allPermission+=a+";";
        Toast.makeText(activity, allPermission+"Permissions Missed!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(mapboxMap,style);
                }
            });
        } else {
            Toast.makeText(activity, "User location permission not granted", Toast.LENGTH_LONG).show();
        }
    }
}
